package com.gauravsaluja.meesho.di.modules;

import com.gauravsaluja.domain.executor.PostExecutionThread;
import com.gauravsaluja.meesho.threading.MainThread;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvidePostExecutionThreadFactory
    implements Factory<PostExecutionThread> {
  private final AppModule module;

  private final Provider<MainThread> uiThreadProvider;

  public AppModule_ProvidePostExecutionThreadFactory(
      AppModule module, Provider<MainThread> uiThreadProvider) {
    assert module != null;
    this.module = module;
    assert uiThreadProvider != null;
    this.uiThreadProvider = uiThreadProvider;
  }

  @Override
  public PostExecutionThread get() {
    return Preconditions.checkNotNull(
        module.providePostExecutionThread(uiThreadProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<PostExecutionThread> create(
      AppModule module, Provider<MainThread> uiThreadProvider) {
    return new AppModule_ProvidePostExecutionThreadFactory(module, uiThreadProvider);
  }

  /** Proxies {@link AppModule#providePostExecutionThread(MainThread)}. */
  public static PostExecutionThread proxyProvidePostExecutionThread(
      AppModule instance, MainThread uiThread) {
    return instance.providePostExecutionThread(uiThread);
  }
}
