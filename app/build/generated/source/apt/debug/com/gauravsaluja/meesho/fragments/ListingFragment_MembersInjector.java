package com.gauravsaluja.meesho.fragments;

import com.gauravsaluja.meesho.adapters.ListingAdapter;
import com.gauravsaluja.meesho.presenters.impl.ListingPresenterImpl;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ListingFragment_MembersInjector implements MembersInjector<ListingFragment> {
  private final Provider<ListingPresenterImpl> listingPresenterProvider;

  private final Provider<ListingAdapter> listingAdapterProvider;

  public ListingFragment_MembersInjector(
      Provider<ListingPresenterImpl> listingPresenterProvider,
      Provider<ListingAdapter> listingAdapterProvider) {
    assert listingPresenterProvider != null;
    this.listingPresenterProvider = listingPresenterProvider;
    assert listingAdapterProvider != null;
    this.listingAdapterProvider = listingAdapterProvider;
  }

  public static MembersInjector<ListingFragment> create(
      Provider<ListingPresenterImpl> listingPresenterProvider,
      Provider<ListingAdapter> listingAdapterProvider) {
    return new ListingFragment_MembersInjector(listingPresenterProvider, listingAdapterProvider);
  }

  @Override
  public void injectMembers(ListingFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.listingPresenter = listingPresenterProvider.get();
    instance.listingAdapter = listingAdapterProvider.get();
  }
}
