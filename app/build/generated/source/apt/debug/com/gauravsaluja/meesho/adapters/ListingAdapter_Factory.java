package com.gauravsaluja.meesho.adapters;

import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ListingAdapter_Factory implements Factory<ListingAdapter> {
  private final MembersInjector<ListingAdapter> listingAdapterMembersInjector;

  public ListingAdapter_Factory(MembersInjector<ListingAdapter> listingAdapterMembersInjector) {
    assert listingAdapterMembersInjector != null;
    this.listingAdapterMembersInjector = listingAdapterMembersInjector;
  }

  @Override
  public ListingAdapter get() {
    return MembersInjectors.injectMembers(listingAdapterMembersInjector, new ListingAdapter());
  }

  public static Factory<ListingAdapter> create(
      MembersInjector<ListingAdapter> listingAdapterMembersInjector) {
    return new ListingAdapter_Factory(listingAdapterMembersInjector);
  }
}
