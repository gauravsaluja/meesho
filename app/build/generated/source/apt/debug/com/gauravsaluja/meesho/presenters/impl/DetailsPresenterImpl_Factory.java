package com.gauravsaluja.meesho.presenters.impl;

import com.gauravsaluja.domain.interactors.GetProductDetailUseCase;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DetailsPresenterImpl_Factory implements Factory<DetailsPresenterImpl> {
  private final Provider<GetProductDetailUseCase> productDetailUseCaseProvider;

  public DetailsPresenterImpl_Factory(
      Provider<GetProductDetailUseCase> productDetailUseCaseProvider) {
    assert productDetailUseCaseProvider != null;
    this.productDetailUseCaseProvider = productDetailUseCaseProvider;
  }

  @Override
  public DetailsPresenterImpl get() {
    return new DetailsPresenterImpl(productDetailUseCaseProvider.get());
  }

  public static Factory<DetailsPresenterImpl> create(
      Provider<GetProductDetailUseCase> productDetailUseCaseProvider) {
    return new DetailsPresenterImpl_Factory(productDetailUseCaseProvider);
  }
}
