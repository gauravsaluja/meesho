package com.gauravsaluja.meesho.di.modules;

import com.gauravsaluja.data.api.ProductApi;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvideProductApiFactory implements Factory<ProductApi> {
  private final NetworkModule module;

  private final Provider<Retrofit> retrofitProvider;

  public NetworkModule_ProvideProductApiFactory(
      NetworkModule module, Provider<Retrofit> retrofitProvider) {
    assert module != null;
    this.module = module;
    assert retrofitProvider != null;
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public ProductApi get() {
    return Preconditions.checkNotNull(
        module.provideProductApi(retrofitProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ProductApi> create(
      NetworkModule module, Provider<Retrofit> retrofitProvider) {
    return new NetworkModule_ProvideProductApiFactory(module, retrofitProvider);
  }

  /** Proxies {@link NetworkModule#provideProductApi(Retrofit)}. */
  public static ProductApi proxyProvideProductApi(NetworkModule instance, Retrofit retrofit) {
    return instance.provideProductApi(retrofit);
  }
}
