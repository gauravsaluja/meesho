// Generated code from Butter Knife. Do not modify!
package com.gauravsaluja.meesho.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gauravsaluja.meesho.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingActivity_ViewBinding implements Unbinder {
  private ListingActivity target;

  @UiThread
  public ListingActivity_ViewBinding(ListingActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListingActivity_ViewBinding(ListingActivity target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar_listing, "field 'toolbar'", Toolbar.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.listing_title, "field 'title'", TextView.class);
    target.drawer = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawer'", DrawerLayout.class);
    target.navigationView = Utils.findRequiredViewAsType(source, R.id.nav_view, "field 'navigationView'", NavigationView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.title = null;
    target.drawer = null;
    target.navigationView = null;
  }
}
