package com.gauravsaluja.domain.interactors;

import com.gauravsaluja.domain.executor.Executor;
import com.gauravsaluja.domain.executor.PostExecutionThread;
import com.gauravsaluja.domain.repository.ProductRepository;
import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GetProductDetailUseCase_Factory implements Factory<GetProductDetailUseCase> {
  private final MembersInjector<GetProductDetailUseCase> getProductDetailUseCaseMembersInjector;

  private final Provider<Executor> threadExecutorProvider;

  private final Provider<PostExecutionThread> postExecutionThreadProvider;

  private final Provider<ProductRepository> repositoryProvider;

  public GetProductDetailUseCase_Factory(
      MembersInjector<GetProductDetailUseCase> getProductDetailUseCaseMembersInjector,
      Provider<Executor> threadExecutorProvider,
      Provider<PostExecutionThread> postExecutionThreadProvider,
      Provider<ProductRepository> repositoryProvider) {
    assert getProductDetailUseCaseMembersInjector != null;
    this.getProductDetailUseCaseMembersInjector = getProductDetailUseCaseMembersInjector;
    assert threadExecutorProvider != null;
    this.threadExecutorProvider = threadExecutorProvider;
    assert postExecutionThreadProvider != null;
    this.postExecutionThreadProvider = postExecutionThreadProvider;
    assert repositoryProvider != null;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public GetProductDetailUseCase get() {
    return MembersInjectors.injectMembers(
        getProductDetailUseCaseMembersInjector,
        new GetProductDetailUseCase(
            threadExecutorProvider.get(),
            postExecutionThreadProvider.get(),
            repositoryProvider.get()));
  }

  public static Factory<GetProductDetailUseCase> create(
      MembersInjector<GetProductDetailUseCase> getProductDetailUseCaseMembersInjector,
      Provider<Executor> threadExecutorProvider,
      Provider<PostExecutionThread> postExecutionThreadProvider,
      Provider<ProductRepository> repositoryProvider) {
    return new GetProductDetailUseCase_Factory(
        getProductDetailUseCaseMembersInjector,
        threadExecutorProvider,
        postExecutionThreadProvider,
        repositoryProvider);
  }
}
