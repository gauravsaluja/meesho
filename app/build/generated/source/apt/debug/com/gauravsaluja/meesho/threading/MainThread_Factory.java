package com.gauravsaluja.meesho.threading;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MainThread_Factory implements Factory<MainThread> {
  private static final MainThread_Factory INSTANCE = new MainThread_Factory();

  @Override
  public MainThread get() {
    return new MainThread();
  }

  public static Factory<MainThread> create() {
    return INSTANCE;
  }
}
