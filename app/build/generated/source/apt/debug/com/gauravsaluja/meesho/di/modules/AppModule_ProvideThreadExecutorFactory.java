package com.gauravsaluja.meesho.di.modules;

import com.gauravsaluja.domain.executor.Executor;
import com.gauravsaluja.domain.executor.impl.ThreadExecutor;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideThreadExecutorFactory implements Factory<Executor> {
  private final AppModule module;

  private final Provider<ThreadExecutor> threadExecutorProvider;

  public AppModule_ProvideThreadExecutorFactory(
      AppModule module, Provider<ThreadExecutor> threadExecutorProvider) {
    assert module != null;
    this.module = module;
    assert threadExecutorProvider != null;
    this.threadExecutorProvider = threadExecutorProvider;
  }

  @Override
  public Executor get() {
    return Preconditions.checkNotNull(
        module.provideThreadExecutor(threadExecutorProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Executor> create(
      AppModule module, Provider<ThreadExecutor> threadExecutorProvider) {
    return new AppModule_ProvideThreadExecutorFactory(module, threadExecutorProvider);
  }

  /** Proxies {@link AppModule#provideThreadExecutor(ThreadExecutor)}. */
  public static Executor proxyProvideThreadExecutor(
      AppModule instance, ThreadExecutor threadExecutor) {
    return instance.provideThreadExecutor(threadExecutor);
  }
}
