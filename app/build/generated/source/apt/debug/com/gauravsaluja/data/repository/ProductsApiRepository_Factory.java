package com.gauravsaluja.data.repository;

import com.gauravsaluja.data.api.ProductApi;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ProductsApiRepository_Factory implements Factory<ProductsApiRepository> {
  private final Provider<ProductApi> productApiProvider;

  public ProductsApiRepository_Factory(Provider<ProductApi> productApiProvider) {
    assert productApiProvider != null;
    this.productApiProvider = productApiProvider;
  }

  @Override
  public ProductsApiRepository get() {
    return new ProductsApiRepository(productApiProvider.get());
  }

  public static Factory<ProductsApiRepository> create(Provider<ProductApi> productApiProvider) {
    return new ProductsApiRepository_Factory(productApiProvider);
  }
}
