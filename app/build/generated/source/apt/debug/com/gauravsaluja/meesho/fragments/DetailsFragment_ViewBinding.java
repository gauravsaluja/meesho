// Generated code from Butter Knife. Do not modify!
package com.gauravsaluja.meesho.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.gauravsaluja.meesho.R;
import com.gauravsaluja.meesho.customviews.CirclePageIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailsFragment_ViewBinding implements Unbinder {
  private DetailsFragment target;

  private View view2131230743;

  @UiThread
  public DetailsFragment_ViewBinding(final DetailsFragment target, View source) {
    this.target = target;

    View view;
    target.detailParentLayout = Utils.findRequiredViewAsType(source, R.id.product_detail_parent_layout, "field 'detailParentLayout'", LinearLayout.class);
    target.progressLoad = Utils.findRequiredViewAsType(source, R.id.progress_load, "field 'progressLoad'", ProgressBar.class);
    target.imagesViewPager = Utils.findRequiredViewAsType(source, R.id.images_pager, "field 'imagesViewPager'", ViewPager.class);
    target.imagesIndicator = Utils.findRequiredViewAsType(source, R.id.images_indicator, "field 'imagesIndicator'", CirclePageIndicator.class);
    target.productName = Utils.findRequiredViewAsType(source, R.id.info_product_name, "field 'productName'", TextView.class);
    target.productQuantity = Utils.findRequiredViewAsType(source, R.id.info_product_quantity, "field 'productQuantity'", TextView.class);
    target.productPromoPrice = Utils.findRequiredViewAsType(source, R.id.info_product_promo_price, "field 'productPromoPrice'", TextView.class);
    target.productPrice = Utils.findRequiredViewAsType(source, R.id.info_product_price, "field 'productPrice'", TextView.class);
    target.containerItemDescription = Utils.findRequiredViewAsType(source, R.id.container_item_description, "field 'containerItemDescription'", LinearLayout.class);
    target.noResultsContainer = Utils.findRequiredViewAsType(source, R.id.no_results_container, "field 'noResultsContainer'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.action_retry, "field 'actionRetry' and method 'loadProductDetails'");
    target.actionRetry = Utils.castView(view, R.id.action_retry, "field 'actionRetry'", TextView.class);
    view2131230743 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.loadProductDetails();
      }
    });
    target.retryText = Utils.findRequiredViewAsType(source, R.id.retry_text, "field 'retryText'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.detailParentLayout = null;
    target.progressLoad = null;
    target.imagesViewPager = null;
    target.imagesIndicator = null;
    target.productName = null;
    target.productQuantity = null;
    target.productPromoPrice = null;
    target.productPrice = null;
    target.containerItemDescription = null;
    target.noResultsContainer = null;
    target.actionRetry = null;
    target.retryText = null;

    view2131230743.setOnClickListener(null);
    view2131230743 = null;
  }
}
