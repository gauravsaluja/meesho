// Generated code from Butter Knife. Do not modify!
package com.gauravsaluja.meesho.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.gauravsaluja.meesho.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PagerImageFragment_ViewBinding implements Unbinder {
  private PagerImageFragment target;

  @UiThread
  public PagerImageFragment_ViewBinding(PagerImageFragment target, View source) {
    this.target = target;

    target.itemCover = Utils.findRequiredViewAsType(source, R.id.item_cover, "field 'itemCover'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PagerImageFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.itemCover = null;
  }
}
