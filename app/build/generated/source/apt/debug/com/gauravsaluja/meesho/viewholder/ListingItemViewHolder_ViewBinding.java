// Generated code from Butter Knife. Do not modify!
package com.gauravsaluja.meesho.viewholder;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.gauravsaluja.meesho.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingItemViewHolder_ViewBinding implements Unbinder {
  private ListingItemViewHolder target;

  private View view2131230835;

  @UiThread
  public ListingItemViewHolder_ViewBinding(final ListingItemViewHolder target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.listing_item_layout, "field 'productInfoLayout' and method 'onProductItemClick'");
    target.productInfoLayout = Utils.castView(view, R.id.listing_item_layout, "field 'productInfoLayout'", ConstraintLayout.class);
    view2131230835 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onProductItemClick();
      }
    });
    target.productImage = Utils.findRequiredViewAsType(source, R.id.product_image, "field 'productImage'", AppCompatImageView.class);
    target.productName = Utils.findRequiredViewAsType(source, R.id.product_name, "field 'productName'", AppCompatTextView.class);
    target.productQuantity = Utils.findRequiredViewAsType(source, R.id.product_quantity, "field 'productQuantity'", AppCompatTextView.class);
    target.productPromoPrice = Utils.findRequiredViewAsType(source, R.id.product_promo_price, "field 'productPromoPrice'", AppCompatTextView.class);
    target.productPrice = Utils.findRequiredViewAsType(source, R.id.product_price, "field 'productPrice'", AppCompatTextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingItemViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.productInfoLayout = null;
    target.productImage = null;
    target.productName = null;
    target.productQuantity = null;
    target.productPromoPrice = null;
    target.productPrice = null;

    view2131230835.setOnClickListener(null);
    view2131230835 = null;
  }
}
