package com.gauravsaluja.domain.interactors;

import com.gauravsaluja.domain.executor.Executor;
import com.gauravsaluja.domain.executor.PostExecutionThread;
import com.gauravsaluja.domain.repository.ProductRepository;
import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GetProductsListUseCase_Factory implements Factory<GetProductsListUseCase> {
  private final MembersInjector<GetProductsListUseCase> getProductsListUseCaseMembersInjector;

  private final Provider<Executor> threadExecutorProvider;

  private final Provider<PostExecutionThread> postExecutionThreadProvider;

  private final Provider<ProductRepository> repositoryProvider;

  public GetProductsListUseCase_Factory(
      MembersInjector<GetProductsListUseCase> getProductsListUseCaseMembersInjector,
      Provider<Executor> threadExecutorProvider,
      Provider<PostExecutionThread> postExecutionThreadProvider,
      Provider<ProductRepository> repositoryProvider) {
    assert getProductsListUseCaseMembersInjector != null;
    this.getProductsListUseCaseMembersInjector = getProductsListUseCaseMembersInjector;
    assert threadExecutorProvider != null;
    this.threadExecutorProvider = threadExecutorProvider;
    assert postExecutionThreadProvider != null;
    this.postExecutionThreadProvider = postExecutionThreadProvider;
    assert repositoryProvider != null;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public GetProductsListUseCase get() {
    return MembersInjectors.injectMembers(
        getProductsListUseCaseMembersInjector,
        new GetProductsListUseCase(
            threadExecutorProvider.get(),
            postExecutionThreadProvider.get(),
            repositoryProvider.get()));
  }

  public static Factory<GetProductsListUseCase> create(
      MembersInjector<GetProductsListUseCase> getProductsListUseCaseMembersInjector,
      Provider<Executor> threadExecutorProvider,
      Provider<PostExecutionThread> postExecutionThreadProvider,
      Provider<ProductRepository> repositoryProvider) {
    return new GetProductsListUseCase_Factory(
        getProductsListUseCaseMembersInjector,
        threadExecutorProvider,
        postExecutionThreadProvider,
        repositoryProvider);
  }
}
