// Generated code from Butter Knife. Do not modify!
package com.gauravsaluja.meesho.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.gauravsaluja.meesho.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingFragment_ViewBinding implements Unbinder {
  private ListingFragment target;

  private View view2131230743;

  @UiThread
  public ListingFragment_ViewBinding(final ListingFragment target, View source) {
    this.target = target;

    View view;
    target.productListingRecyclerView = Utils.findRequiredViewAsType(source, R.id.product_listing, "field 'productListingRecyclerView'", RecyclerView.class);
    target.progressLoad = Utils.findRequiredViewAsType(source, R.id.progress_load, "field 'progressLoad'", ProgressBar.class);
    target.noResultsContainer = Utils.findRequiredViewAsType(source, R.id.no_results_container, "field 'noResultsContainer'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.action_retry, "field 'actionRetry' and method 'loadProductList'");
    target.actionRetry = Utils.castView(view, R.id.action_retry, "field 'actionRetry'", TextView.class);
    view2131230743 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.loadProductList();
      }
    });
    target.retryText = Utils.findRequiredViewAsType(source, R.id.retry_text, "field 'retryText'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.productListingRecyclerView = null;
    target.progressLoad = null;
    target.noResultsContainer = null;
    target.actionRetry = null;
    target.retryText = null;

    view2131230743.setOnClickListener(null);
    view2131230743 = null;
  }
}
