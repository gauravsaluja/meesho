package com.gauravsaluja.meesho.fragments;

import com.gauravsaluja.meesho.presenters.impl.DetailsPresenterImpl;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DetailsFragment_MembersInjector implements MembersInjector<DetailsFragment> {
  private final Provider<DetailsPresenterImpl> detailsPresenterProvider;

  public DetailsFragment_MembersInjector(Provider<DetailsPresenterImpl> detailsPresenterProvider) {
    assert detailsPresenterProvider != null;
    this.detailsPresenterProvider = detailsPresenterProvider;
  }

  public static MembersInjector<DetailsFragment> create(
      Provider<DetailsPresenterImpl> detailsPresenterProvider) {
    return new DetailsFragment_MembersInjector(detailsPresenterProvider);
  }

  @Override
  public void injectMembers(DetailsFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.detailsPresenter = detailsPresenterProvider.get();
  }
}
