package com.gauravsaluja.domain.executor.impl;

import dagger.internal.Factory;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ThreadExecutor_Factory implements Factory<ThreadExecutor> {
  private static final ThreadExecutor_Factory INSTANCE = new ThreadExecutor_Factory();

  @Override
  public ThreadExecutor get() {
    return new ThreadExecutor();
  }

  public static Factory<ThreadExecutor> create() {
    return INSTANCE;
  }
}
