package com.gauravsaluja.meesho.di.modules;

import com.gauravsaluja.data.repository.ProductsApiRepository;
import com.gauravsaluja.domain.repository.ProductRepository;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class AppModule_ProvideProductRepositoryFactory implements Factory<ProductRepository> {
  private final AppModule module;

  private final Provider<ProductsApiRepository> productsApiRepositoryProvider;

  public AppModule_ProvideProductRepositoryFactory(
      AppModule module, Provider<ProductsApiRepository> productsApiRepositoryProvider) {
    assert module != null;
    this.module = module;
    assert productsApiRepositoryProvider != null;
    this.productsApiRepositoryProvider = productsApiRepositoryProvider;
  }

  @Override
  public ProductRepository get() {
    return Preconditions.checkNotNull(
        module.provideProductRepository(productsApiRepositoryProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ProductRepository> create(
      AppModule module, Provider<ProductsApiRepository> productsApiRepositoryProvider) {
    return new AppModule_ProvideProductRepositoryFactory(module, productsApiRepositoryProvider);
  }

  /** Proxies {@link AppModule#provideProductRepository(ProductsApiRepository)}. */
  public static ProductRepository proxyProvideProductRepository(
      AppModule instance, ProductsApiRepository productsApiRepository) {
    return instance.provideProductRepository(productsApiRepository);
  }
}
