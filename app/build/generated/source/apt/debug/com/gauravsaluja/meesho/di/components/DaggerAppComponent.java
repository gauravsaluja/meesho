package com.gauravsaluja.meesho.di.components;

import android.app.Application;
import com.gauravsaluja.data.api.ProductApi;
import com.gauravsaluja.data.repository.ProductsApiRepository;
import com.gauravsaluja.data.repository.ProductsApiRepository_Factory;
import com.gauravsaluja.domain.executor.Executor;
import com.gauravsaluja.domain.executor.PostExecutionThread;
import com.gauravsaluja.domain.executor.impl.ThreadExecutor_Factory;
import com.gauravsaluja.domain.repository.ProductRepository;
import com.gauravsaluja.meesho.di.modules.AppModule;
import com.gauravsaluja.meesho.di.modules.AppModule_ProvidePostExecutionThreadFactory;
import com.gauravsaluja.meesho.di.modules.AppModule_ProvideProductRepositoryFactory;
import com.gauravsaluja.meesho.di.modules.AppModule_ProvideThreadExecutorFactory;
import com.gauravsaluja.meesho.di.modules.AppModule_ProvidesApplicationFactory;
import com.gauravsaluja.meesho.di.modules.NetworkModule;
import com.gauravsaluja.meesho.di.modules.NetworkModule_ProvideGsonFactory;
import com.gauravsaluja.meesho.di.modules.NetworkModule_ProvideOkHttpCacheFactory;
import com.gauravsaluja.meesho.di.modules.NetworkModule_ProvideOkHttpClientFactory;
import com.gauravsaluja.meesho.di.modules.NetworkModule_ProvideProductApiFactory;
import com.gauravsaluja.meesho.di.modules.NetworkModule_ProvideRetrofitFactory;
import com.gauravsaluja.meesho.threading.MainThread;
import com.gauravsaluja.meesho.threading.MainThread_Factory;
import com.google.gson.Gson;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerAppComponent implements AppComponent {
  private Provider<Application> providesApplicationProvider;

  private Provider<Executor> provideThreadExecutorProvider;

  private Provider<MainThread> mainThreadProvider;

  private Provider<PostExecutionThread> providePostExecutionThreadProvider;

  private Provider<Gson> provideGsonProvider;

  private Provider<Cache> provideOkHttpCacheProvider;

  private Provider<OkHttpClient> provideOkHttpClientProvider;

  private Provider<Retrofit> provideRetrofitProvider;

  private Provider<ProductApi> provideProductApiProvider;

  private Provider<ProductsApiRepository> productsApiRepositoryProvider;

  private Provider<ProductRepository> provideProductRepositoryProvider;

  private DaggerAppComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.providesApplicationProvider =
        DoubleCheck.provider(AppModule_ProvidesApplicationFactory.create(builder.appModule));

    this.provideThreadExecutorProvider =
        DoubleCheck.provider(
            AppModule_ProvideThreadExecutorFactory.create(
                builder.appModule, ThreadExecutor_Factory.create()));

    this.mainThreadProvider = DoubleCheck.provider(MainThread_Factory.create());

    this.providePostExecutionThreadProvider =
        DoubleCheck.provider(
            AppModule_ProvidePostExecutionThreadFactory.create(
                builder.appModule, mainThreadProvider));

    this.provideGsonProvider =
        DoubleCheck.provider(NetworkModule_ProvideGsonFactory.create(builder.networkModule));

    this.provideOkHttpCacheProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideOkHttpCacheFactory.create(
                builder.networkModule, providesApplicationProvider));

    this.provideOkHttpClientProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideOkHttpClientFactory.create(
                builder.networkModule, provideOkHttpCacheProvider));

    this.provideRetrofitProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideRetrofitFactory.create(
                builder.networkModule, provideGsonProvider, provideOkHttpClientProvider));

    this.provideProductApiProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideProductApiFactory.create(
                builder.networkModule, provideRetrofitProvider));

    this.productsApiRepositoryProvider =
        ProductsApiRepository_Factory.create(provideProductApiProvider);

    this.provideProductRepositoryProvider =
        DoubleCheck.provider(
            AppModule_ProvideProductRepositoryFactory.create(
                builder.appModule, productsApiRepositoryProvider));
  }

  @Override
  public Application providesApplication() {
    return providesApplicationProvider.get();
  }

  @Override
  public Executor provideThreadExecutor() {
    return provideThreadExecutorProvider.get();
  }

  @Override
  public PostExecutionThread providePostExecutionThread() {
    return providePostExecutionThreadProvider.get();
  }

  @Override
  public ProductRepository provideProductRepository() {
    return provideProductRepositoryProvider.get();
  }

  @Override
  public Cache provideOkHttpCache() {
    return provideOkHttpCacheProvider.get();
  }

  @Override
  public Gson provideGson() {
    return provideGsonProvider.get();
  }

  @Override
  public OkHttpClient provideOkHttpClient() {
    return provideOkHttpClientProvider.get();
  }

  @Override
  public ProductApi provideProductApi() {
    return provideProductApiProvider.get();
  }

  public static final class Builder {
    private AppModule appModule;

    private NetworkModule networkModule;

    private Builder() {}

    public AppComponent build() {
      if (appModule == null) {
        throw new IllegalStateException(AppModule.class.getCanonicalName() + " must be set");
      }
      if (networkModule == null) {
        throw new IllegalStateException(NetworkModule.class.getCanonicalName() + " must be set");
      }
      return new DaggerAppComponent(this);
    }

    public Builder appModule(AppModule appModule) {
      this.appModule = Preconditions.checkNotNull(appModule);
      return this;
    }

    public Builder networkModule(NetworkModule networkModule) {
      this.networkModule = Preconditions.checkNotNull(networkModule);
      return this;
    }
  }
}
