package com.gauravsaluja.meesho.di.modules;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.Cache;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvideOkHttpCacheFactory implements Factory<Cache> {
  private final NetworkModule module;

  private final Provider<Application> applicationProvider;

  public NetworkModule_ProvideOkHttpCacheFactory(
      NetworkModule module, Provider<Application> applicationProvider) {
    assert module != null;
    this.module = module;
    assert applicationProvider != null;
    this.applicationProvider = applicationProvider;
  }

  @Override
  public Cache get() {
    return Preconditions.checkNotNull(
        module.provideOkHttpCache(applicationProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Cache> create(
      NetworkModule module, Provider<Application> applicationProvider) {
    return new NetworkModule_ProvideOkHttpCacheFactory(module, applicationProvider);
  }

  /** Proxies {@link NetworkModule#provideOkHttpCache(Application)}. */
  public static Cache proxyProvideOkHttpCache(NetworkModule instance, Application application) {
    return instance.provideOkHttpCache(application);
  }
}
