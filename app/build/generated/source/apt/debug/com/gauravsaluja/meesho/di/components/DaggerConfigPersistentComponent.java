package com.gauravsaluja.meesho.di.components;

import com.gauravsaluja.domain.executor.Executor;
import com.gauravsaluja.domain.executor.PostExecutionThread;
import com.gauravsaluja.domain.interactors.GetProductDetailUseCase;
import com.gauravsaluja.domain.interactors.GetProductDetailUseCase_Factory;
import com.gauravsaluja.domain.interactors.GetProductsListUseCase;
import com.gauravsaluja.domain.interactors.GetProductsListUseCase_Factory;
import com.gauravsaluja.domain.repository.ProductRepository;
import com.gauravsaluja.meesho.adapters.ListingAdapter;
import com.gauravsaluja.meesho.adapters.ListingAdapter_Factory;
import com.gauravsaluja.meesho.base.BaseActivity;
import com.gauravsaluja.meesho.di.modules.ActivityModule;
import com.gauravsaluja.meesho.di.modules.FragmentModule;
import com.gauravsaluja.meesho.fragments.DetailsFragment;
import com.gauravsaluja.meesho.fragments.DetailsFragment_MembersInjector;
import com.gauravsaluja.meesho.fragments.ListingFragment;
import com.gauravsaluja.meesho.fragments.ListingFragment_MembersInjector;
import com.gauravsaluja.meesho.presenters.impl.DetailsPresenterImpl;
import com.gauravsaluja.meesho.presenters.impl.DetailsPresenterImpl_Factory;
import com.gauravsaluja.meesho.presenters.impl.ListingPresenterImpl;
import com.gauravsaluja.meesho.presenters.impl.ListingPresenterImpl_Factory;
import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerConfigPersistentComponent implements ConfigPersistentComponent {
  private Provider<Executor> provideThreadExecutorProvider;

  private Provider<PostExecutionThread> providePostExecutionThreadProvider;

  private Provider<ProductRepository> provideProductRepositoryProvider;

  private DaggerConfigPersistentComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.provideThreadExecutorProvider =
        new Factory<Executor>() {
          private final AppComponent appComponent = builder.appComponent;

          @Override
          public Executor get() {
            return Preconditions.checkNotNull(
                appComponent.provideThreadExecutor(),
                "Cannot return null from a non-@Nullable component method");
          }
        };

    this.providePostExecutionThreadProvider =
        new Factory<PostExecutionThread>() {
          private final AppComponent appComponent = builder.appComponent;

          @Override
          public PostExecutionThread get() {
            return Preconditions.checkNotNull(
                appComponent.providePostExecutionThread(),
                "Cannot return null from a non-@Nullable component method");
          }
        };

    this.provideProductRepositoryProvider =
        new Factory<ProductRepository>() {
          private final AppComponent appComponent = builder.appComponent;

          @Override
          public ProductRepository get() {
            return Preconditions.checkNotNull(
                appComponent.provideProductRepository(),
                "Cannot return null from a non-@Nullable component method");
          }
        };
  }

  @Override
  public ActivityComponent activityComponent(ActivityModule activityModule) {
    return new ActivityComponentImpl(activityModule);
  }

  @Override
  public FragmentComponent fragmentComponent(FragmentModule fragmentModule) {
    return new FragmentComponentImpl(fragmentModule);
  }

  public static final class Builder {
    private AppComponent appComponent;

    private Builder() {}

    public ConfigPersistentComponent build() {
      if (appComponent == null) {
        throw new IllegalStateException(AppComponent.class.getCanonicalName() + " must be set");
      }
      return new DaggerConfigPersistentComponent(this);
    }

    public Builder appComponent(AppComponent appComponent) {
      this.appComponent = Preconditions.checkNotNull(appComponent);
      return this;
    }
  }

  private final class ActivityComponentImpl implements ActivityComponent {
    private final ActivityModule activityModule;

    private ActivityComponentImpl(ActivityModule activityModule) {
      this.activityModule = Preconditions.checkNotNull(activityModule);
    }

    @Override
    public void inject(BaseActivity baseActivity) {
      MembersInjectors.<BaseActivity>noOp().injectMembers(baseActivity);
    }
  }

  private final class FragmentComponentImpl implements FragmentComponent {
    private final FragmentModule fragmentModule;

    private Provider<GetProductsListUseCase> getProductsListUseCaseProvider;

    private Provider<ListingPresenterImpl> listingPresenterImplProvider;

    private Provider<ListingAdapter> listingAdapterProvider;

    private MembersInjector<ListingFragment> listingFragmentMembersInjector;

    private Provider<GetProductDetailUseCase> getProductDetailUseCaseProvider;

    private Provider<DetailsPresenterImpl> detailsPresenterImplProvider;

    private MembersInjector<DetailsFragment> detailsFragmentMembersInjector;

    private FragmentComponentImpl(FragmentModule fragmentModule) {
      this.fragmentModule = Preconditions.checkNotNull(fragmentModule);
      initialize();
    }

    @SuppressWarnings("unchecked")
    private void initialize() {

      this.getProductsListUseCaseProvider =
          GetProductsListUseCase_Factory.create(
              MembersInjectors.<GetProductsListUseCase>noOp(),
              DaggerConfigPersistentComponent.this.provideThreadExecutorProvider,
              DaggerConfigPersistentComponent.this.providePostExecutionThreadProvider,
              DaggerConfigPersistentComponent.this.provideProductRepositoryProvider);

      this.listingPresenterImplProvider =
          ListingPresenterImpl_Factory.create(getProductsListUseCaseProvider);

      this.listingAdapterProvider =
          ListingAdapter_Factory.create(MembersInjectors.<ListingAdapter>noOp());

      this.listingFragmentMembersInjector =
          ListingFragment_MembersInjector.create(
              listingPresenterImplProvider, listingAdapterProvider);

      this.getProductDetailUseCaseProvider =
          GetProductDetailUseCase_Factory.create(
              MembersInjectors.<GetProductDetailUseCase>noOp(),
              DaggerConfigPersistentComponent.this.provideThreadExecutorProvider,
              DaggerConfigPersistentComponent.this.providePostExecutionThreadProvider,
              DaggerConfigPersistentComponent.this.provideProductRepositoryProvider);

      this.detailsPresenterImplProvider =
          DetailsPresenterImpl_Factory.create(getProductDetailUseCaseProvider);

      this.detailsFragmentMembersInjector =
          DetailsFragment_MembersInjector.create(detailsPresenterImplProvider);
    }

    @Override
    public void inject(ListingFragment listingFragment) {
      listingFragmentMembersInjector.injectMembers(listingFragment);
    }

    @Override
    public void inject(DetailsFragment detailsFragment) {
      detailsFragmentMembersInjector.injectMembers(detailsFragment);
    }
  }
}
