package com.gauravsaluja.meesho.presenters.impl;

import com.gauravsaluja.domain.interactors.GetProductsListUseCase;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ListingPresenterImpl_Factory implements Factory<ListingPresenterImpl> {
  private final Provider<GetProductsListUseCase> productsListUseCaseProvider;

  public ListingPresenterImpl_Factory(
      Provider<GetProductsListUseCase> productsListUseCaseProvider) {
    assert productsListUseCaseProvider != null;
    this.productsListUseCaseProvider = productsListUseCaseProvider;
  }

  @Override
  public ListingPresenterImpl get() {
    return new ListingPresenterImpl(productsListUseCaseProvider.get());
  }

  public static Factory<ListingPresenterImpl> create(
      Provider<GetProductsListUseCase> productsListUseCaseProvider) {
    return new ListingPresenterImpl_Factory(productsListUseCaseProvider);
  }
}
