package com.gauravsaluja.meesho.utils

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.RelativeSizeSpan
import android.view.View
import android.view.ViewTreeObserver
import android.widget.TextView
import com.gauravsaluja.meesho.customviews.CustomSpannable

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Helper class
 */

object Helpers {

    // get typeface nunito bold
    fun getNunitoTypeFaceBold(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/nunito_bold.ttf")
    }

    // get typeface nunito regular
    fun getNunitoTypeFaceRegular(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/nunito_regular.ttf")
    }

    // get typeface nunito light
    fun getNunitoTypeFaceLight(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/nunito_light.ttf")
    }

    // remove global layout listener
    private fun removeOnGlobalLayoutListener(v: View, listener: ViewTreeObserver.OnGlobalLayoutListener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            v.viewTreeObserver.removeGlobalOnLayoutListener(listener)
        } else {
            v.viewTreeObserver.removeOnGlobalLayoutListener(listener)
        }
    }

    // allow the text view to be resizable by checking if current line count is greater than MAX_LINES
    // add spannable string and click listener on the textview
    fun makeTextViewResizable(context: Context, tv: TextView, maxLine: Int,
                              expandText: String, viewMore: Boolean) {

        if (tv.tag == null) {
            tv.tag = tv.text
        }

        val vto = tv.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {

            override fun onGlobalLayout() {
                val l = tv.layout
                if (l != null) {
                    val lines = l.lineCount
                    if (lines > Constants.MAX_LINES) {
                        if (maxLine > 0 && tv.lineCount >= maxLine) {
                            val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                            val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                            tv.text = text
                            tv.movementMethod = LinkMovementMethod.getInstance()

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tv.setText(addClickablePartTextViewResizable(context, Html.fromHtml(tv.text.toString(), Html.FROM_HTML_MODE_LEGACY),
                                        tv, expandText, viewMore), TextView.BufferType.SPANNABLE)
                            } else {
                                tv.setText(addClickablePartTextViewResizable(context, Html.fromHtml(tv.text.toString()),
                                        tv, expandText, viewMore), TextView.BufferType.SPANNABLE)
                            }
                        } else {
                            val lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                            val text = tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                            tv.text = text
                            tv.movementMethod = LinkMovementMethod.getInstance()

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tv.setText(addClickablePartTextViewResizable(context, Html.fromHtml(tv.text.toString(), Html.FROM_HTML_MODE_LEGACY),
                                        tv, expandText, viewMore), TextView.BufferType.SPANNABLE)
                            } else {
                                tv.setText(addClickablePartTextViewResizable(context, Html.fromHtml(tv.text.toString()),
                                        tv, expandText, viewMore), TextView.BufferType.SPANNABLE)
                            }
                        }
                    }
                }

                removeOnGlobalLayoutListener(tv, this)
            }
        })
    }

    // setup spannable string and add click listener on textview
    private fun addClickablePartTextViewResizable(context: Context, strSpanned: Spanned, tv: TextView,
                                                  spannableText: String, viewMore: Boolean): SpannableStringBuilder {

        val str = strSpanned.toString()
        val ssb = SpannableStringBuilder(strSpanned)

        if (!TextUtils.isEmpty(spannableText) && str.contains(spannableText)) {
            ssb.setSpan(object : CustomSpannable(context, false) {
                override fun onClick(widget: View) {

                }
            }, str.indexOf(spannableText), str.indexOf(spannableText) + spannableText.length, 0)

            ssb.setSpan(RelativeSizeSpan(1.1f), str.indexOf(spannableText), str.indexOf(spannableText) + spannableText.length, 0)
        }

        tv.setOnClickListener { onClickDescription(context, tv, viewMore) }

        return ssb
    }

    // click logic of textview
    private fun onClickDescription(context: Context, tv: TextView, viewMore: Boolean) {
        tv.layoutParams = tv.layoutParams
        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
        tv.invalidate()

        if (viewMore) {
            makeTextViewResizable(context, tv, Integer.MAX_VALUE, "", false)
        } else {
            makeTextViewResizable(context, tv, Constants.MAX_LINES, Constants.READ_MORE_STRING, true)
        }
    }
}