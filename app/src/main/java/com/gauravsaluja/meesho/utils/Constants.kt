package com.gauravsaluja.meesho.utils

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Constants class
 */

object Constants {

    const val BASE_IMAGE_URL = "http://media.redmart.com/newmedia/200p"
    const val READ_MORE_STRING = " ... more"
    const val MAX_LINES = 3
}