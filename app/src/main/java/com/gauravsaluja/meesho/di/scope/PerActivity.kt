package com.gauravsaluja.meesho.di.scope

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Scope

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Activity scope
 */

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity