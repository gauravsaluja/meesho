package com.gauravsaluja.meesho.di.components;

import com.gauravsaluja.meesho.di.modules.FragmentModule;
import com.gauravsaluja.meesho.di.scope.PerFragment;
import com.gauravsaluja.meesho.fragments.DetailsFragment;
import com.gauravsaluja.meesho.fragments.ListingFragment;

import dagger.Subcomponent;

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Fragment component
 */

@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

    void inject(ListingFragment listingFragment);

    void inject(DetailsFragment detailsFragment);
}