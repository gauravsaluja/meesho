package com.gauravsaluja.meesho.di.components;

import android.app.Application;

import com.gauravsaluja.data.api.ProductApi;
import com.gauravsaluja.domain.executor.Executor;
import com.gauravsaluja.domain.executor.PostExecutionThread;
import com.gauravsaluja.domain.repository.ProductRepository;
import com.gauravsaluja.meesho.di.modules.AppModule;
import com.gauravsaluja.meesho.di.modules.NetworkModule;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Application component
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {

    Application providesApplication();

    Executor provideThreadExecutor();

    PostExecutionThread providePostExecutionThread();

    ProductRepository provideProductRepository();

    Cache provideOkHttpCache();

    Gson provideGson();

    OkHttpClient provideOkHttpClient();

    ProductApi provideProductApi();
}