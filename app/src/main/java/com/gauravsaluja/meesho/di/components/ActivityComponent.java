package com.gauravsaluja.meesho.di.components;

import com.gauravsaluja.meesho.base.BaseActivity;
import com.gauravsaluja.meesho.di.modules.ActivityModule;
import com.gauravsaluja.meesho.di.scope.PerActivity;

import dagger.Subcomponent;

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Activity component
 */

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);
}