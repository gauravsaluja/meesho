package com.gauravsaluja.meesho.di.components;

import com.gauravsaluja.meesho.di.modules.ActivityModule;
import com.gauravsaluja.meesho.di.modules.FragmentModule;
import com.gauravsaluja.meesho.di.scope.ConfigPersistent;

import dagger.Component;

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 * <p>
 * Configuration persistent component
 */

@ConfigPersistent
@Component(dependencies = AppComponent.class)
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);
}