package com.gauravsaluja.meesho.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 *
 * View holder for load more progress bar
 */

class ListingLoadMoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)