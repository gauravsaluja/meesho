package com.gauravsaluja.meesho.customviews

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

import com.gauravsaluja.meesho.R
import com.gauravsaluja.meesho.utils.Helpers

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Custom clickable span to remove underline
 */

open class CustomSpannable(private val context: Context, private val isUnderline: Boolean) : ClickableSpan() {

    // set color and typeface
    override fun updateDrawState(ds: TextPaint) {
        ds.isUnderlineText = isUnderline
        ds.color = ContextCompat.getColor(context, R.color.colorPrimary)
        ds.typeface = Helpers.getNunitoTypeFaceRegular(context)
    }

    override fun onClick(widget: View) {

    }
}