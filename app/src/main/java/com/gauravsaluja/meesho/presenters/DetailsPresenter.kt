package com.gauravsaluja.meesho.presenters

import com.gauravsaluja.domain.model.ProductDetailResponse
import com.gauravsaluja.meesho.base.BasePresenter

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Contract logic for product detail
 */

interface DetailsPresenter : BasePresenter {

    // functions exposed to DetailsFragment
    interface View {
        fun onProductDetailLoading()

        fun onProductDetailLoaded(detailResponse: ProductDetailResponse)

        fun onProductDetailFailed()
    }

    // function to bind view and contract
    fun setView(view: View)

    // load function for getting product detail
    @Throws(Exception::class)
    fun load(productId: Int)
}