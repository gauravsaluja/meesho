package com.gauravsaluja.meesho.presenters

import com.gauravsaluja.domain.model.ProductListingResponse
import com.gauravsaluja.meesho.base.BasePresenter

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Contract logic for product listing
 */

interface ListingPresenter : BasePresenter {

    // functions exposed to ListingFragment
    interface View {
        fun onProductListingLoading()

        fun onProductListingLoaded(listingResponse: ProductListingResponse)

        fun onProductListingFailed()
    }

    // function to bind view and contract
    fun setView(view: View)

    // load function for getting product listing
    @Throws(Exception::class)
    fun load(page: Int)
}