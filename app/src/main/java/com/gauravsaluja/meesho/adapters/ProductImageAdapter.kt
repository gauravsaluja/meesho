package com.gauravsaluja.meesho.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.SparseArray
import android.view.ViewGroup

import com.gauravsaluja.domain.model.Image
import com.gauravsaluja.meesho.fragments.PagerImageFragment

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Adapter for product image view pager
 */

class ProductImageAdapter(fm: FragmentManager, private val images: List<Image>) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return PagerImageFragment.newInstance(images[position])
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        registeredFragments.put(position, fragment)
        return fragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        registeredFragments.remove(position)
        super.destroyItem(container, position, `object`)
    }

    companion object {
        private val registeredFragments = SparseArray<Fragment>()
    }
}