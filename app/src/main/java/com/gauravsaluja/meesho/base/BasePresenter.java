package com.gauravsaluja.meesho.base;

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Base class for presenters
 */

public interface BasePresenter {

    void resume();

    void pause();

    void stop();

    void destroy();
}