package com.gauravsaluja.meesho.threading

import android.os.Handler
import android.os.Looper

import com.gauravsaluja.domain.executor.PostExecutionThread

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 *
 * Implementation of post execution thread which will provide main / UI thread
 */

@Singleton
class MainThread @Inject
constructor() : PostExecutionThread {

    override fun getScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}