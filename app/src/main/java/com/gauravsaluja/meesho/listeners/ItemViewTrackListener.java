package com.gauravsaluja.meesho.listeners;

import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 * <p>
 * Track item visible events
 */

public class ItemViewTrackListener {

    private static final int THRESHOLD_MS = 300;

    private Subject<VisibleState> publishSubject;
    private Disposable subscription;
    private final Consumer<VisibleState> onSuccess;
    private HashSet<Integer> trackEvents = new HashSet<>();

    public ItemViewTrackListener(final Consumer<VisibleState> onSuccess,
                                 final Consumer<Throwable> onError) {

        this.onSuccess = onSuccess;
        this.publishSubject = PublishSubject.create();
        this.subscription = publishSubject
                .distinctUntilChanged()
                .throttleWithTimeout(THRESHOLD_MS, TimeUnit.MILLISECONDS)
                .subscribe(this::onCallback, onError);
    }

    public void postViewEvent(final VisibleState visibleState) {
        publishSubject.onNext(visibleState);
    }

    public void unsubscribe() {
        subscription.dispose();
    }

    private void onCallback(VisibleState visibleState) {
        int firstVisible = visibleState.firstVisible;
        int lastVisible = visibleState.lastVisible;

        VisibleState visibleStateFinal;

        if (trackEvents.size() == 0) {
            if (firstVisible <= lastVisible) {
                for (int i = firstVisible; i <= lastVisible; i++) {
                    trackEvents.add(i);
                }
            }

            visibleStateFinal = new VisibleState(firstVisible, lastVisible);

            try {
                onSuccess.accept(visibleStateFinal);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            HashSet<Integer> updatedTrackEvents = new HashSet<>(trackEvents);
            trackEvents.clear();

            if (firstVisible <= lastVisible) {
                for (int i = firstVisible; i <= lastVisible; i++) {
                    if (!updatedTrackEvents.contains(i)) {
                        trackEvents.add(i);
                    } else {
                        updatedTrackEvents.remove(i);
                    }
                }
            }

            if (trackEvents.size() > 0) {
                visibleStateFinal = new VisibleState(trackEvents);

                try {
                    onSuccess.accept(visibleStateFinal);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                trackEvents.addAll(updatedTrackEvents);
            }
        }
    }

    public static class VisibleState {
        int firstVisible = -1;
        int lastVisible = -1;
        HashSet<Integer> visibleItemSet = new HashSet<>();

        public VisibleState(int firstVisibleItem,
                            int lastVisibleItem) {

            this.firstVisible = firstVisibleItem;
            this.lastVisible = lastVisibleItem;
        }

        public VisibleState(HashSet<Integer> itemSet) {
            this.visibleItemSet.addAll(itemSet);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VisibleState that = (VisibleState) o;

            if (firstVisible != that.firstVisible) return false;
            return lastVisible == that.lastVisible;
        }

        @Override
        public int hashCode() {
            int result = firstVisible;
            result = 31 * result + lastVisible;
            return result;
        }

        @Override
        public String toString() {
            String trackingRequests = "";

            if (visibleItemSet.size() > 0) {
                for (Integer integer : visibleItemSet) {
                    trackingRequests = trackingRequests.concat("Tracking item at position: " + integer);
                    trackingRequests = trackingRequests.concat("\n");
                }
            } else {
                if (firstVisible <= lastVisible) {
                    for (int i = firstVisible; i <= lastVisible; i++) {
                        trackingRequests = trackingRequests.concat("Tracking item at position: " + i);
                        trackingRequests = trackingRequests.concat("\n");
                    }
                }
            }

            return trackingRequests;
        }
    }
}