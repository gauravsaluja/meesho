package com.gauravsaluja.meesho.listeners;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.HashSet;

/**
 * Created by Gaurav Saluja on 22-Oct-18.
 * <p>
 * Pagination listener to intercept next page load and apply logic for next page load
 */

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;
    private ItemViewTrackListener viewTrackListener;
    private HashSet<Integer> trackEvents = new HashSet<>();

    protected PaginationScrollListener(LinearLayoutManager layoutManager, ItemViewTrackListener trackListener) {
        this.layoutManager = layoutManager;
        this.viewTrackListener = trackListener;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int firstVisible = layoutManager.findFirstVisibleItemPosition();
        int lastVisible = layoutManager.findLastVisibleItemPosition();

        ItemViewTrackListener.VisibleState visibleStateFinal = new ItemViewTrackListener.VisibleState(firstVisible, lastVisible);
        viewTrackListener.postViewEvent(visibleStateFinal);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        // logic for next page load
        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0) {

                loadMoreItems();
            }
        }
    }

    protected abstract void loadMoreItems();

    protected abstract boolean isLastPage();

    protected abstract boolean isLoading();
}