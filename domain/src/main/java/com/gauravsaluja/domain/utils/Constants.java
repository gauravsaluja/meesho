package com.gauravsaluja.domain.utils;

/**
 * Created by Gaurav Saluja on 24-Oct-18.
 *
 * Constants file for domain module
 */

public class Constants {

    public static final String BASE_URL = "https://api.redmart.com/v1.6.0/catalog/";
}