package com.gauravsaluja.domain.interactors;

import com.gauravsaluja.domain.executor.Executor;
import com.gauravsaluja.domain.executor.PostExecutionThread;
import com.gauravsaluja.domain.model.ProductListingResponse;
import com.gauravsaluja.domain.repository.ProductRepository;
import io.reactivex.Observable;

import javax.inject.Inject;
import java.util.Map;

/**
 * Created by Gaurav Saluja on 24-Oct-18.
 *
 * Use case for getting product list
 */

public class GetProductsListUseCase extends UseCase<ProductListingResponse, Map<String, Object>> {

    private ProductRepository productRepository;

    @Inject
    public GetProductsListUseCase(Executor threadExecutor, PostExecutionThread postExecutionThread,
                                  ProductRepository repository) {

        super(threadExecutor, postExecutionThread);
        this.productRepository = repository;
    }

    // build observable for getting product list response
    @Override
    public Observable<ProductListingResponse> buildUseCaseObservable(Map<String, Object> queryParams) {
        return productRepository.getProductsList(queryParams);
    }
}