package com.gauravsaluja.domain.executor;

import io.reactivex.Scheduler;

/**
 * Created by Gaurav Saluja on 24-Oct-18.
 *
 * PostExecutionThread interface, to be used for main / UI threads
 */

public interface PostExecutionThread {

    Scheduler getScheduler();
}