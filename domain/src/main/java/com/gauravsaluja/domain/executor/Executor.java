package com.gauravsaluja.domain.executor;

/**
 * Created by Gaurav Saluja on 24-Oct-18.
 *
 * Executor interface, to be used for background threads
 */

public interface Executor extends java.util.concurrent.Executor {

}