package com.gauravsaluja.domain.repository;

import com.gauravsaluja.domain.model.ProductDetailResponse;
import com.gauravsaluja.domain.model.ProductListingResponse;
import io.reactivex.Observable;

import java.util.Map;

/**
 * Created by Gaurav Saluja on 24-Oct-18.
 *
 * Repository interface
 */

public interface ProductRepository {

    // get products list
    Observable<ProductListingResponse> getProductsList(Map<String, Object> queryParams);

    // get product detail
    Observable<ProductDetailResponse> getProductDetail(Integer productId);
}